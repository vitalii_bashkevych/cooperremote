package controller;

import java.io.IOException;
import java.util.HashMap;

import general.Client;
import general.CommandReader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import util.Updater;

public class CooperMWController{
	private Client client = Client.getCLIENT();
	private HashMap<String, String> commandsMap;
	CommandReader commandReader = new CommandReader();
	
	public CooperMWController(){
		commandsMap = commandReader.getComandsMap();
	}

	@FXML
	 private void buttonClicked(ActionEvent event) {
		String buttonId = getButtonId(event.getSource().toString());
        sendCommand(buttonId);
	}
	 
	 
	@FXML
	 private void updateCommands(ActionEvent event) {
		String buttonId = getButtonId(event.getSource().toString());
		if(buttonId.equals("update")) {

			Updater updater = new Updater();
			updater.showUpdaterDialog();
		}
		else {
			
		}
	}

	private String getButtonId(String action) {
		String buttonId = action.substring(action.indexOf("id=")+3, action.indexOf(", "));
		System.out.println("Button = "+buttonId);
		return buttonId;
	}
	
	 void sendCommand(String buttonId){
		 try {
			 
			 client.sendCommand(commandsMap.get(buttonId));

		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	 }
	 

}
