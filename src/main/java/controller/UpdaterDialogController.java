package controller;

import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import general.XMLgenerator;

public class UpdaterDialogController {
	private XMLgenerator xmlGenerator = new XMLgenerator();

	@FXML
	TextField pathField = new TextField();
	
	@FXML
	 private void buttonClicked(ActionEvent event) {
		String buttonId = getButtonId(event.getSource().toString());
		
		if(buttonId.equals("goButton")) {
    		String folderPath = pathField.getText();
    		System.out.println("pathField = "+pathField.getText());
     		HashMap<String, String> map = xmlGenerator.readFiles(folderPath);
    		if(!map.isEmpty()) xmlGenerator.generatXML(map);
    		
    		pathField.clear();
		}
		
	}

	private String getButtonId(String action) {
		String buttonId = action.substring(action.indexOf("id=")+3, action.indexOf(", "));
		System.out.println("B_id = "+buttonId);
		return buttonId;
	}
	
	

}
