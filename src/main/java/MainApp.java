

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;


public class MainApp  extends Application{
    private Stage primaryStage;
    private AnchorPane Main;

	@Override
	public void start(Stage primaryStage) {
		
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Remote");
		this.primaryStage.setResizable(false);
		//this.primaryStage.setAlwaysOnTop(true);
		this.primaryStage.setMinHeight(310);
		this.primaryStage.setMinWidth(160); 
		
		FlowPane rootNode = new FlowPane();
        ImageView imgView = new ImageView(new Image(getClass().getResource("img/icon.png").toString()));
        System.out.println("pict = "+getClass().getResource("img/icon.png").toString());
        
        primaryStage.getIcons().add(new Image(getClass().getResource("img/icon.png").toString()));
		
		rootNode.setAlignment(Pos.CENTER);
		
		initMain();

	}
	
    public void initMain() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/CooperMW.fxml"));
            Main = (AnchorPane) loader.load();
            Scene scene = new Scene(Main);
            
            primaryStage.setScene(scene);
            primaryStage.show();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
  
    }
    
    
    
	public static void main(String[] args) {
		launch(args);

	}
	

}
