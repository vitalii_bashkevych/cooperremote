package general;


import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.scene.control.Alert;


public class Client {
	private static DatagramSocket SOCKET;
	private static DatagramPacket PACKET;
    private static String SERVER_IP;
    private static final int PORT = 7000;

    private static Client CLIENT;
    private Alert alert = new Alert(Alert.AlertType.INFORMATION);
		 
    private boolean wasException = false;
    
    private Client() {
    	
	}


	public void sendCommand (String comand) throws InterruptedException, IOException {
  			if(comand!=null) {
    			SOCKET = new DatagramSocket(51630);
     			if(SERVER_IP==null | wasException) SERVER_IP = findServerIP();
                
            	String datA1 = "{\"cid\":\"app\",\"i\":1,\"pack\":\"";
            	String datA2 = "{\"cid\":\"app\",\"i\":0,\"pack\":\"";
            	String datZ = "\",\"t\":\"pack\",\"tcid\":\"f4911e09fb92\",\"uid\":1021894}";
            	String verif1 = "7pULMPRM0pKpXQoMIozq9dUFQ8r64GV2yWizHkkJOWmj9mrs/0p7GUTdWxmNaBUt";
            	
            	String verif2  = "MtFro/QLgn0pyN+lnN9NjzuKnvg5FX63LYTd2cBVEbZQS/KOhMWU9gpp"+
            			"TmwZ35ZKtLKBTkKaz/Ak/w57vGMHs4SGKvzgpfJKTsEmy5M9+DV7cP6cApNWtmqhl"+
            			"ZWZHaIXWBGIURDuXig5ghyD/fB+60/Me4jsY9N9esN7hgUTOFZF4vIgd/ZLBN/cky"+
            			"m7vkEwUkVt0/ZXSBNmxQsQ1V8m3QjH208fsUjMY9AjChprkTt2+gfQWR8NCBQdCfz"+
            			"uItIAEM70nrjnOUiVbLNkVuMSrA==";
    	    
            try {
             	
            	sendPacket(SERVER_IP,datA1 +verif1+ datZ);
            	resivePacket();
            	Thread.sleep(300);
            	
            	sendPacket(SERVER_IP,datA2 +verif2+ datZ);
            	resivePacket();
            	Thread.sleep(300);
            	
            	sendPacket(SERVER_IP,datA2 +comand+ datZ);
            	resivePacket();
         
            } finally {
    			SOCKET.close();
    		}
        }
  			else {
  				alert.setTitle("Information");
                alert.setHeaderText(null);
                alert.setContentText(" Текст ");
                alert.showAndWait();
      		}	
   }


	

    private String findServerIP(){
		String serverIP = "127.0.0.1";
		String mask ="255.255.255.255";
		String scan ="{\"t\":\"scan\"}";

		String socketAddress;
    	sendPacket(mask, scan);

    	 if ((resivePacket()!= null)){
             socketAddress = PACKET.getSocketAddress().toString();
             serverIP = socketAddress.substring(socketAddress.indexOf('/') + 1, socketAddress.indexOf(':'));
     		 System.out.println(serverIP);
    	 }
    	 return serverIP;
    }

    
    

	private void sendPacket(String ip, String data)  {
       	byte [] command = data.getBytes();

		try {
			InetAddress inetAddress = InetAddress.getByName(ip);
			DatagramPacket  datagramPacket = new DatagramPacket(command, command.length, inetAddress, PORT);
			SOCKET.send(datagramPacket);
 			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
       System.out.println( "\n" + data);
	}
    

	
	
    private String resivePacket(){
        String result  = "null";
    	byte[] buf = new byte[1000]; 
    	PACKET = new DatagramPacket(buf, buf.length);
    	try {
    		SOCKET.setSoTimeout(2000); // время ожидания 
    		SOCKET.receive(PACKET);
    		
		} catch (SocketException| SocketTimeoutException e) {
			wasException = true;
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"There was no devices found \n or device does not respond!");
		} catch (IOException e) {
			wasException = true;
			e.printStackTrace();
		}
   	
         result = new String(PACKET.getData());
         System.out.println(result);
         return result;
   }



	public static Client getCLIENT() {
		if(CLIENT == null) {
			CLIENT = new Client();
		}
		return CLIENT;
	}
 
}
    

