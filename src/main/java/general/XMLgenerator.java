package general;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javafx.scene.control.Alert;

public class XMLgenerator implements FileFilter{
    private Alert alert;
	private String filePath =  "src/commands123456.xml";


	
	
	public ArrayList<File> getFileList(String folderPath){
		ArrayList<File> listWithFile = new ArrayList<>();
		File file = new File(folderPath);
		try {
    		for (File s : file.listFiles()) {
                 if (s.isFile()&accept(s)) {
                    listWithFile.add(s);
                 } else if (s.isDirectory()) {
                	 getFileList(s.getAbsolutePath());      
                 }
    		}
				
		} catch (Exception e) {
			alert = new Alert(Alert.AlertType.ERROR);
			String requst = "";
			alert.setTitle("Information");
	        alert.setHeaderText(null);
	        alert.setContentText("Файли не найдено!");
	        alert.showAndWait();

		}
	return listWithFile;
	}
	
	
	
	public HashMap<String, String> readFiles(String folderPath) {
		HashMap<String, String> map = new HashMap<>();
		ArrayList<File> fileList = getFileList(folderPath);
		for (File file : fileList) {
			if(!fileList.isEmpty())
    			try {
    				String s1 = new String(Files.readAllBytes(Paths.get(file.getCanonicalPath())));
    				
    				s1 = validateData(s1);
    				
    				map.put(file.getName(), s1);
    				
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
			else {
/*				alert = new Alert(Alert.AlertType.ERROR);
				String requst = "";
				alert.setTitle("Information");
		        alert.setHeaderText(null);
		        alert.setContentText("Файли не найдено!");
		        alert.showAndWait();
*/				break;
			}
		}
		return map;
	}
	
	
	
	private String validateData(String s1) {
		if(s1.indexOf("{")>=0 & s1.indexOf("}")>0) {
    		s1 = s1.substring(s1.indexOf("{"), s1.indexOf("}")+1);
    		return s1;
		}
		else return ""; //TODO  
	}

	
	
	
	public void generatXML(HashMap<String, String>  map){
		File file = new File(filePath);

	    if(!file.exists()){
	    	wrF(map);
	    }
	     else {
	    	 if(displayAlert("Файл: "+file.getAbsolutePath()+" уже существует!\n Заменить!").equals("OK_DONE")){
	    		 file.delete();
	    		 wrF(map);
	    	 }    	 
	     }
	}
	
	void wrF(HashMap<String,String> list){
		File file = new File(filePath);
	    PrintWriter out = null;
    	try {
    		out = new PrintWriter(file.getAbsoluteFile());
    		file.createNewFile();
    		out.print(
    				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
    				"<cooper_remote_commands>\r\n" + 
    				"	<buttons>\r\n");
    		Set<String> set = list.keySet();
    		System.out.println( set.size());
    		
   		
    		for (String string : set) {
       			out.print(
           			"		<button>\r\n" +
    				"			<id>"+string+"</id><data>"+list.get(string)+"</data>\r\n" + 
    				"		</button>\r\n");

        			System.out.println(list.get(string));
 			}
    		
    		out.print(
    				"	</buttons>\r\n" + 
    				"</cooper_remote_commands>");
	    } 
    	catch(IOException e) {
    		e.printStackTrace();
	    } 
	    finally {
	    	out.close();
	    }
	}
	
	
	private String displayAlert (String text) {
		alert = new Alert(Alert.AlertType.CONFIRMATION);
		String requst = "";
		alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
		return requst = alert.getResult().getButtonData().toString();
	}


	@Override
	public boolean accept(File pathname) {
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		if(pathname.getName().matches("2"))  return false; 
		else if(pathname.getName().length()>30) return false; 
		else return true;
	}
	
	
	

}
