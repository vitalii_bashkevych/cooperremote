package general;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CommandReader {
	private static HashMap<String, String> hashMap;
	public static String filePath = "dat/commands.xml";
	public static ArrayList <String> list = new ArrayList<>();

	
	
	public HashMap getComandsMap() {
		if(hashMap==null) {
			list = takeDataFromFile();
			hashMap = convertLictToMap(list);
		}
		return hashMap;
	}
	
	
	
	private ArrayList<String> takeDataFromFile() {
		ArrayList <String> list = new ArrayList<>();
		Path path = Paths.get("src/main/resources/dat/commands.xml");
		System.out.println("My path = "+path.toAbsolutePath().toString());
		try {
			list.addAll(Files.readAllLines(path.toAbsolutePath()));
		} catch (IOException  e1) {
			e1.printStackTrace();
		} 
		
		return list;
	}

	
	
	private static HashMap<String, String> convertLictToMap(List<String> list){
		HashMap<String, String> hashMap = new HashMap<>();
		for (String string : list) {
			if(string.indexOf("<id>")>0) {
    			String id = string.substring(string.indexOf("<id>")+4, string.indexOf("</id>"));
    			String pack = string.substring(string.indexOf("<data>")+6, string.indexOf("</data>")); 
    			System.out.println(id+"   ==   "+pack);
    			hashMap.put(id, pack);
			}
		}
		return hashMap;
	}
}
