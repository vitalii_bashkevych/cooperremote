package util;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Updater{
	private static String title = "Updater";
	private static String iconPath = "../img/update.png";
	private static String fxmlPath = "../view/UpdaterDialog.fxml";

	

	public boolean showUpdaterDialog() {
	    try {
	        FXMLLoader loader = new FXMLLoader();
	        Image icon = new Image(getClass().getResource(iconPath).toString());
	        ImageView imgView = new ImageView(icon);
	        loader.setLocation(Updater.class.getResource(fxmlPath));
	        
	        AnchorPane page = (AnchorPane) loader.load();
	        
	        Scene scene = new Scene(page);
	        
	        Stage updaterDialigStage = new Stage();

	        updaterDialigStage.setTitle(title);
	        updaterDialigStage.setResizable(false);
	        //updaterDialigStage.setAlwaysOnTop(true);
	        updaterDialigStage.setScene(scene);
	        updaterDialigStage.getIcons().add(icon);
	        updaterDialigStage.showAndWait();
	        
	        
	        
	        
	        
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
		return false;
	}



	public static String getTitle() {
		return title;
	}



	public static String getIconPath() {
		return iconPath;
	}



	public static String getFxmlPath() {
		return fxmlPath;
	}



	public static void setTitle(String title) {
		Updater.title = title;
	}



	public static void setIconPath(String iconPath) {
		Updater.iconPath = iconPath;
	}



	public static void setFxmlPath(String fxmlPath) {
		Updater.fxmlPath = fxmlPath;
	}
	
	

}



